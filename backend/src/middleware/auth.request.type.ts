import { Request } from 'express'
import { Types as MongooseTypes } from 'mongoose'
export type AuthRequest = Request & {
  user: {
    _id: MongooseTypes.ObjectId
    fullName: string
  }
}
