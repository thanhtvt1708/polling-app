import { Model, PopulateOptions, SortOrder } from 'mongoose'
import { PaginationQueryDto } from './pagination.dto'
import { PaginatedDto } from './paginated.dto'
import * as _ from 'lodash'
export async function paginatedMongoQuery<T>({
  model,
  query,
  page,
  populatePaths,
}: {
  model: Model<T>
  query: Record<string, unknown>
  page?: PaginationQueryDto
  populatePaths?: (string | PopulateOptions)[]
}): Promise<PaginatedDto<T>> {
  const { $sort, $select, $skip: skip = 0, $limit: limit = 20 } = page || {}

  if (page) query = _.omit(query, Object.keys(page))

  const total = await model.find(query).count().exec()

  const selectPresent = $select?.length
  const populate = populatePaths?.map(field => {
    if (typeof field === 'string') {
      if (selectPresent && _.findIndex($select, o => o === field) !== -1)
        return { path: field }
      else return { path: field }
    } else if (typeof field === 'object') {
      if (selectPresent && _.findIndex($select, o => o === field.path) !== -1)
        return field
      else return field
    }
  })

  let mongoQuery = model.find(query)
  $sort && (mongoQuery = mongoQuery.sort($sort as { [key: string]: SortOrder }))
  mongoQuery = mongoQuery.skip(skip).limit(limit)
  $select && (mongoQuery = mongoQuery.select($select))

  const data = await mongoQuery.populate(_.compact(populate)).exec()
  return { total, skip, limit, data }
}
