import {
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
  MaxLength,
} from 'class-validator'

export class NewUser {
  @MaxLength(64)
  @IsString()
  @IsNotEmpty()
  fullName: string

  @IsEmail()
  @IsNotEmpty()
  email: string

  @Matches(
    /^(?=.*[A-Z])(?=.*[~`!@#$%^&*()-_+={}[\]|\\/:;"'<>,.?])(?=.*[0-9])(?=.*[a-z]).{8,}$/,
  )
  @IsString()
  @IsNotEmpty()
  password: string
}
