import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { AuthRequest } from '~/middleware'
import { UserService } from '~/user/user.service'
import { IS_PUBLIC_KEY } from '~/util/decorators'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly userService: UserService,
    private readonly reflector: Reflector,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const getMetadata = <T>(key: string) =>
      this.reflector.getAllAndOverride<T>(key, [
        context.getHandler(),
        context.getClass(),
      ])

    const isPublic = getMetadata<boolean>(IS_PUBLIC_KEY) ?? false
    if (isPublic) return true
    const request: AuthRequest = context.switchToHttp().getRequest()
    const user = await this.userService.findById(request.user._id)
    request.user = user
    if (user) return true
  }
}
