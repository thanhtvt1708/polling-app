import {
  MessageBody,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets'
import { Socket, Server } from 'socket.io'
import { Model, Types as MongooseTypes } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'
import { Polling, pollingDocument } from '~/schemas'
@WebSocketGateway({
  cors: {
    origin: process.env.HOST_SOCKET_ALLOWED,
    credentials: true,
  },
})
export class SocketGateway implements OnGatewayInit, OnGatewayDisconnect {
  constructor(
    @InjectModel(Polling.name)
    private readonly pollingModel: Model<pollingDocument>,
  ) {}
  @WebSocketServer() server: Server
  @SubscribeMessage('new-vote')
  public async votePolly(
    @MessageBody() data: { pollyId: MongooseTypes.ObjectId },
  ): Promise<void> {
    const { pollyId } = data
    const polly = await this.pollingModel
      .findOne({ _id: pollyId })
      .populate({
        path: 'owner',
        select: 'fullName',
      })
      .populate({
        path: 'options',
        populate: {
          path: 'voters',
          select: 'fullName',
        },
      })
      .exec()
    this.server.emit(`voted_${pollyId}`, polly)
    this.server.emit('update', polly)
    return
  }

  @SubscribeMessage('new-polly')
  public async newPolly(
    @MessageBody() data: { polly: pollingDocument },
  ): Promise<void> {
    const { polly } = data
    this.server.emit('new-polly-create', polly)
    return
  }
  public afterInit(): void {
    console.log('server started.........................')
  }
  public handleConnection(client: Socket): void {
    client.emit('user', client.id)
    console.log('client++++++++++++++++++++++++++++++++', client.id)
  }
  public handleDisconnect(client: Socket): void {
    console.log('client===================================', client.id)
  }
}
