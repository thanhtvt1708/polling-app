import { Module } from '@nestjs/common'
import { SocketGateway } from './socket.gateway'
import { MongooseModule } from '@nestjs/mongoose'
import { Polling, pollingSchema } from '~/schemas'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Polling.name, schema: pollingSchema }]),
  ],
  providers: [SocketGateway],
})
export class SocketModule {}
