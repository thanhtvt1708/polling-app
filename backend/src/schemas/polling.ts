import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types as MongooseTypes, SchemaTimestampsConfig } from 'mongoose'
import { User } from './user'
import { DocumentWithId } from './types'
export type pollingDocument = Polling & DocumentWithId & SchemaTimestampsConfig
@Schema({ _id: false })
export class OptionsPolly {
  @Prop({ type: String, required: true })
  optionName: string

  @Prop({ type: [MongooseTypes.ObjectId], required: false, ref: User.name })
  voters: MongooseTypes.ObjectId[]
}

const OptionsPollySchema = SchemaFactory.createForClass(OptionsPolly)

@Schema({ collection: 'polling', timestamps: true })
export class Polling {
  @Prop({ type: String, required: true })
  title: string

  @Prop({ type: [OptionsPollySchema], required: true })
  options: OptionsPolly[]

  @Prop({ type: MongooseTypes.ObjectId, required: true, ref: User.name })
  owner: MongooseTypes.ObjectId
}

export const pollingSchema = SchemaFactory.createForClass(Polling)
