import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types as MongooseTypes, SchemaTimestampsConfig } from 'mongoose'
import { DocumentWithId } from './types'
export type userDocument = User & DocumentWithId & SchemaTimestampsConfig
@Schema({ collection: 'users', timestamps: true })
export class User {
  @Prop({ type: String, required: true })
  fullName: string

  @Prop({ type: String, required: true, unique: true })
  email: string

  @Prop({ type: String, required: true })
  password: string

  @Prop({ type: [MongooseTypes.ObjectId], required: false })
  votedPolly?: MongooseTypes.ObjectId[]
}

export const userSchema = SchemaFactory.createForClass(User)
