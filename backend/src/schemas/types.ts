import { Document, Types } from 'mongoose'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type DocumentWithId<TQueryHelpers = any, DocType = any> = Omit<
  Document<Types.ObjectId, TQueryHelpers, DocType>,
  '_id'
> & { _id: Types.ObjectId }
