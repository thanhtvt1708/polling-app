import { createMock } from '@golevelup/ts-jest'
import {
  Polling,
  pollingDocument,
  pollingSchema,
  userDocument,
} from '~/schemas'
import { PollyService } from './polly.service'
import { Test, TestingModule } from '@nestjs/testing'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MongooseModule, getModelToken } from '@nestjs/mongoose'
import { UserModule } from '~/user/user.module'
import { Model, Types as MongooseTypes } from 'mongoose'
import { NewPolly } from './dto/new-polly.dto'
import { VotePolly } from './dto/vote-polly.dto'
import { UserService } from '~/user/user.service'
import { UserNotAllowedException } from '~/exceptions'

describe('PollyService', () => {
  let service: PollyService
  let module: TestingModule
  let pollingModel: Model<pollingDocument>
  let userService: UserService

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.env'],
        }),
        MongooseModule.forRootAsync({
          inject: [ConfigService],
          useFactory: (config: ConfigService) => ({
            uri: config.get<string>('MONGODB_URL'),
          }),
        }),
        MongooseModule.forFeature([
          { name: Polling.name, schema: pollingSchema },
        ]),
        UserModule,
      ],
      providers: [PollyService],
    }).compile()

    service = module.get<PollyService>(PollyService)
    userService = module.get<UserService>(UserService)
    pollingModel = module.get<Model<pollingDocument>>(
      getModelToken(Polling.name),
    )
  })

  afterEach(async () => {
    jest.clearAllMocks()
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('create new polly', () => {
    const newPolly = createMock<NewPolly>({
      title: 'string',
    })
    const currentUser = createMock<userDocument>()
    const polly = createMock<pollingDocument>({ title: 'string' })
    const fnMock = jest.fn().mockReturnValue(polly)
    it('it create successfully', async () => {
      jest.spyOn(pollingModel, 'create').mockImplementation(fnMock)
      const result = await service.createPolly(newPolly, currentUser)
      expect(result).toBeTruthy()
    })
  })

  describe('vote polly', () => {
    const votePolly = createMock<VotePolly>({
      pollyId: new MongooseTypes.ObjectId('6374a846c66037dfc1269d5d'),
      optionNames: ['a'],
    })
    const currentUser = createMock<userDocument>({
      _id: new MongooseTypes.ObjectId('6541e18aefebc66bd451ab09'),
      votedPolly: [],
    })
    const polly = createMock<pollingDocument>({
      _id: new MongooseTypes.ObjectId('6374a846c66037dfc1269d5d'),
      options: [{ optionName: 'a', voters: [] }],
    })
    const fnMock = jest.fn().mockReturnValue(polly)
    it('it vote successfully', async () => {
      jest.spyOn(service, 'checkExistedPolly').mockImplementation(fnMock)
      jest.spyOn(pollingModel, 'updateOne').mockImplementation()
      jest.spyOn(userService, 'update').mockImplementation()
      const result = await service.votePolly(votePolly, currentUser)
      expect(result).toMatchObject({ message: 'success' })
    })

    it('it throw user not allowed', async () => {
      currentUser.votedPolly = [
        new MongooseTypes.ObjectId('6374a846c66037dfc1269d5d'),
      ]
      jest.spyOn(service, 'checkExistedPolly').mockImplementation(fnMock)
      try {
        await service.votePolly(votePolly, currentUser)
      } catch (error) {
        expect(error instanceof UserNotAllowedException)
      }
    })

    it('it throw user not allowed', async () => {
      currentUser._id = new MongooseTypes.ObjectId('6374a846c66037dfc1269d5e')
      polly.owner = new MongooseTypes.ObjectId('6374a846c66037dfc1269d5e')
      jest.spyOn(service, 'checkExistedPolly').mockImplementation(fnMock)
      try {
        await service.votePolly(votePolly, currentUser)
      } catch (error) {
        expect(error instanceof UserNotAllowedException)
      }
    })
  })
})
