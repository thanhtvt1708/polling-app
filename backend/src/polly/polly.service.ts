import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { FilterQuery, Types as MongooseTypes } from 'mongoose'
import { Model } from 'mongoose'
import { Polling, pollingDocument, userDocument } from '~/schemas'
import { NewPolly } from './dto/new-polly.dto'
import { VotePolly } from './dto/vote-polly.dto'
import { PollyNotFoundException, UserNotAllowedException } from '~/exceptions'
import { PaginationQueryDto } from '~/util/pagination.dto'
import { paginatedMongoQuery } from '~/util/helper'
import { UserService } from '~/user/user.service'

@Injectable()
export class PollyService {
  constructor(
    @InjectModel(Polling.name)
    private readonly pollingModel: Model<pollingDocument>,
    private readonly userService: UserService,
  ) {}

  async checkExistedPolly(id: MongooseTypes.ObjectId) {
    return await this.pollingModel
      .findById(id)
      .exec()
      .then(v => {
        if (!v) throw new PollyNotFoundException()
        return v
      })
  }

  async findAll(
    query: FilterQuery<pollingDocument>,
    pagination: PaginationQueryDto,
  ) {
    return await paginatedMongoQuery({
      model: this.pollingModel,
      query: query,
      page: pagination,
      populatePaths: [
        {
          path: 'owner',
          select: 'fullName',
        },
        {
          path: 'options',
          populate: {
            path: 'voters',
            select: 'fullName',
          },
        },
      ],
    })
  }

  async findOnePolly(id: MongooseTypes.ObjectId) {
    return await this.pollingModel
      .findOne({ _id: id })
      .populate({
        path: 'owner',
        select: 'fullName',
      })
      .populate({
        path: 'options',
        populate: {
          path: 'voters',
          select: 'fullName',
        },
      })
      .exec()
      .then(v => {
        if (!v) throw new PollyNotFoundException()
        return v
      })
  }

  async createPolly(newPolly: NewPolly, user: userDocument) {
    return this.pollingModel.create({ ...newPolly, owner: user._id })
  }

  async votePolly(votePolly: VotePolly, user: userDocument) {
    const { pollyId, optionNames } = votePolly
    const validPolly = await this.checkExistedPolly(pollyId)
    if (user._id.toString() === validPolly.owner.toString())
      throw new UserNotAllowedException()
    if (user.votedPolly && user.votedPolly.includes(pollyId))
      throw new UserNotAllowedException()
    validPolly.options.forEach(e => {
      if (optionNames.includes(e.optionName))
        e.voters.includes(user._id) || e.voters.push(user._id)
    })
    await this.pollingModel.updateOne(
      { _id: pollyId },
      { $set: { options: validPolly.options } },
      { new: true },
    )
    await this.userService.update(
      {
        votedPolly: user.votedPolly ? [pollyId] : user.votedPolly.push(pollyId),
      },
      user,
    )
    return { message: 'success' }
  }
}
