import { IsOptional, IsString } from 'class-validator'

export class QueryPolly {
  @IsString()
  @IsOptional()
  title: string
}
