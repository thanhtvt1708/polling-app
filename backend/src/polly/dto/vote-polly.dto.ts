import { ArrayMinSize, IsArray, IsMongoId, IsNotEmpty } from 'class-validator'
import { Types as MongooseTypes } from 'mongoose'
export class VotePolly {
  @IsMongoId()
  @IsNotEmpty()
  pollyId: MongooseTypes.ObjectId

  @ArrayMinSize(1)
  @IsArray()
  @IsNotEmpty()
  optionNames: string[]
}
