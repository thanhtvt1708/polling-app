import { Type } from 'class-transformer'
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator'
import { OptionsPolly } from '~/schemas'
export class NewPolly {
  @IsString()
  @IsNotEmpty()
  title: string

  @ValidateNested()
  @Type(() => OptionsPolly)
  @IsNotEmpty()
  options: OptionsPolly[]
}
