import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { Polling, pollingSchema } from '~/schemas'
import { PollyController } from './polly.controller'
import { PollyService } from './polly.service'
import { UserModule } from '~/user/user.module'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Polling.name, schema: pollingSchema }]),
    UserModule,
  ],
  controllers: [PollyController],
  providers: [PollyService],
})
export class PollyModule {}
