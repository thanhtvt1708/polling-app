import {
  Body,
  Controller,
  Get,
  Injectable,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common'
import { Types as MongooseTypes } from 'mongoose'
import { NewPolly } from './dto/new-polly.dto'
import { CurrentUser, Pagination } from '~/util/decorators'
import { userDocument } from '~/schemas'
import { PollyService } from './polly.service'
import { VotePolly } from './dto/vote-polly.dto'
import { QueryPolly } from './dto/query-polly.dto'
import { PaginationQueryDto } from '~/util/pagination.dto'

@Injectable()
@Controller('polly')
export class PollyController {
  constructor(private readonly pollyService: PollyService) {}

  @Get('list-polly')
  async findAll(
    @Query() query: QueryPolly,
    @Pagination() pagination: PaginationQueryDto,
  ) {
    return await this.pollyService.findAll(query, pagination)
  }
  @Get(':pollyId')
  async findOnePolly(@Param('pollyId') pollyId: MongooseTypes.ObjectId) {
    return await this.pollyService.findOnePolly(pollyId)
  }
  @Post('new-polly')
  async create(@Body() newPolly: NewPolly, @CurrentUser() user: userDocument) {
    return await this.pollyService.createPolly(newPolly, user)
  }

  @Put('vote-polly')
  async vote(@Body() vote: VotePolly, @CurrentUser() user: userDocument) {
    return await this.pollyService.votePolly(vote, user)
  }
}
