import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Types as MongooseTypes, UpdateQuery } from 'mongoose'
import { Model } from 'mongoose'
import { UserExistedException, UserNotFoundException } from '~/exceptions'
import { User, userDocument } from '~/schemas'

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<userDocument>,
  ) {}

  async create(newUser: User) {
    return await this.userModel.create(newUser)
  }
  async findByEmail(email: string) {
    return await this.userModel
      .findOne({ email: email })
      .exec()
      .then(v => {
        if (!v) throw new UserNotFoundException()
        return v
      })
  }

  async findById(id: MongooseTypes.ObjectId) {
    return await this.userModel
      .findById(id)
      .select(['_id', 'fullName', 'email', 'votedPolly'])
      .exec()
      .then(v => {
        if (!v) throw new UserNotFoundException()
        return v
      })
  }

  async checkUserExisted(email: string) {
    return await this.userModel
      .findOne({ email: email })
      .exec()
      .then(v => {
        if (v) throw new UserExistedException()
      })
  }

  async update(update: UpdateQuery<userDocument>, user: userDocument) {
    return await this.userModel.updateOne(
      { _id: user._id },
      { $addToSet: { ...update } },
    )
  }
}
