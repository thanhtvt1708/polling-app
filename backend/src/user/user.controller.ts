import { Controller, Get, Injectable } from '@nestjs/common'
import { userDocument } from '~/schemas'
import { CurrentUser } from '~/util/decorators'

@Injectable()
@Controller('user')
export class UserController {
  constructor() {}

  @Get('my-profile')
  async getProfile(@CurrentUser() user: userDocument) {
    return user
  }
}
