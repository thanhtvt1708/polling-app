export const ERROR_CODE = {
  INTERNAL_SERVER_ERROR: 500,
  VALIDATION_ERROR: 200404,
  USER_NOT_FOUND: 200404,
  POLLY_NOT_FOUND: 201404,
  USER_EXISTED: 200405,
  USER_NOT_ALLOWED: 200406,
  TOKEN_EXPIRED: 401401,
  TOKEN_INVALID: 401402,
  CREDENTIAL_INCORRECT: 401403,
}

export type AppError = {
  code: number
  debug?: {
    message: string
    details: Record<string, string>
  }
}
