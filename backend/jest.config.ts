const {pathsToModuleNameMapper} = require('ts-jest')
const {compilerOptions} = require('./tsconfig.json')

/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  collectCoverageFrom: ['**/*.(t|j)s'],
  moduleFileExtensions: ['js', 'json', 'ts'],
  moduleNameMapper: {
    ...pathsToModuleNameMapper(compilerOptions.paths, {prefix: '<rootDir>/'})
  },
  rootDir: '.',
  roots: ['<rootDir>/src/'],
  testEnvironment: 'node',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest'
  },
  transformIgnorePatterns: ['^.+\\.js$'],
  verbose: true,
  coverageDirectory: './coverage',
  coveragePathIgnorePatterns: ['main.ts', 'swagger.ts'],
}
