import React from "react";
import { LogIn } from "./page/signIn/login";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { io } from 'socket.io-client';
import { SignUp } from "./page/signUp/signUp";
import { HomePolly } from "./page/home/homePage"
import { Polly } from "./page/polly/polly";
// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";
// Bootstrap Bundle JS
import "bootstrap/dist/js/bootstrap.bundle.min";
import { ProtectedHeader } from "./component/Header";
const socket = io('http://localhost:3500', {transports : ["websocket", "polling"],  withCredentials: true});
const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/signIn" element={<LogIn />}></Route>
        <Route path="/signUp" element={<SignUp />}></Route>
        <Route path="/" element={<ProtectedHeader/>}>
          <Route path="/" element={<HomePolly socket={socket}/>}></Route>
          <Route path="/polly/:pollyId" element={<Polly socket={socket}/>}></Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
