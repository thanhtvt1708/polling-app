import axios from "axios";
const BASE_URL = "http://localhost:3500/";
export async function callHttp(path, method, data, token) {
  try {
    const result = await axios.request({
      baseURL: BASE_URL,
      url: path,
      method,
      data,
      headers: { Authorization: `Bearer ${token}` },
    });
    if ([200, 201].includes(result.status)) return result.data;
  } catch (error) {
    if (!error.response) alert("Something Went Wrong !!!");
    else if (error.response.data.code === 200404) alert("Validate Error");
    else if (error.response.data.statusCode === 401)
      window.location = "/signIn";
    else alert(error.response.message || error.response.data.debug);
  }
}
