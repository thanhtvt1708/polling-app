import { Outlet, useNavigate } from "react-router-dom";
import { withAuthRequired } from "../hoc/withAuthRequired";

export function Header(props) {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate("/");
  };

  const handleLogout = () => {
    localStorage.removeItem('token')
    navigate("/signIn");
  };

  return (
    <>
      <div
        className="container shadow-lg p-3 mb-3 bg-body rounded" style={{padding: 0}}
      >
        <div className="d-flex flex-row justify-content-between">
        <button
          type="button"
          class="btn btn-primary"
          onClick={() => handleClick()}
        >
          {" "}
          LOGO POLLY
        </button>
        <button
          type="button"
          class="btn btn-danger"
          onClick={() => handleLogout()}
        >
          {" "}
          LOG OUT
        </button>
        </div>
      </div>

      <Outlet />
    </>
  );
}

export const ProtectedHeader = withAuthRequired(Header)
