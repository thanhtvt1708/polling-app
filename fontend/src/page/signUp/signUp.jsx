import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { callHttp } from "../../API/apiCall";

export function SignUp(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [fullName, setFullName] = useState("");

  const navigate = useNavigate();

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleFullNameChange = (e) => {
    setFullName(e.target.value);
  };

  const handleSignUp = async (email, password, fullName) => {
      const result = await callHttp("auth/sign-up", "post", {
        email,
        password,
        fullName,
      });
      if(result) navigate("/signIn");
  };

  return (
    <>
    <form class="container-lg position-absolute top-50 start-50 translate-middle">
    <div class="raw d-flex align-self-center justify-content-center align-middle">
        <div class="col-md-6 col-10 shadow p-3 mb-5 bg-body rounded" id="form">
            <h1 class="text-center">Sign Up</h1>
            <div class="form-floating mb-3">
            <input type="text" class="form-control text" id="floatingtext"  placeholder="fullName" onChange={handleFullNameChange} required/>
            <label for="floatingInput">FullName</label>
            </div>
            <div class="form-floating mb-3">
                <input type="email" class="form-control email" id="floatingInput"  placeholder="name@example.com" onChange={handleEmailChange}  required/>
                <label for="floatingInput">Email Address</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control password" id="floatingPassword"  placeholder="Password" onChange={handlePasswordChange} required/>
                <label for="floatingPassword">Password</label>
            </div>
            <div class="button mt-3">
                <button type="button" class="btn btn-success w-100" onClick={() => handleSignUp(email, password, fullName)}>SignUp</button>
                <button class="float-end btn btn-link" onClick={() => navigate("/signIn")}>I have already an account <i class="fa fa-hand-peace-o" style= {{fontSize:"24x", color:"red"}}></i></button>
            </div>
        </div>
    </div>
    </form>
    </>
  );
}
