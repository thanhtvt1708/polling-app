import { useState, useEffect } from "react";
import { callHttp } from "../../API/apiCall";
import { useNavigate } from "react-router-dom";
import { loadingPage } from "../../component/loading";
export function HomePolly({ socket }) {
  const [title, setTitle] = useState("");
  const [inputFields, setInputFields] = useState(["", ""]);
  const [pollyList, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const handleTitleChange = (e) => {
    setTitle(e.target.value);
  };
  const navigate = useNavigate();
  const handleAddFields = () => {
    const newInputFields = inputFields.concat("");
    if (newInputFields.length > 5) alert("The number of answers is limited");
    else setInputFields(newInputFields);
  };

  const handleInputChange = (index, event) => {
    const values = [...inputFields];
    values[index] = event.target.value;
    setInputFields(values);
  };

  const handleSubmit = async (title, inputFields) => {
    if (!inputFields.filter((e) => e).length)
      alert("The Answers Is Not Empty !!!");
    else {
      const options = inputFields
        .filter((e) => e)
        .map((e) => ({ optionName: e }));
      const result = await callHttp(
        "polly/new-polly",
        "post",
        {
          title,
          options,
        },
        localStorage.getItem("token")
      );
      if (result) {
        var modalBackdrop = document.querySelector(".modal-backdrop");
        if (modalBackdrop) {
          modalBackdrop.parentNode.removeChild(modalBackdrop);
        }
        socket.emit("new-polly", { polly: result });
        navigate(`/polly/${result._id}`);
      }
    }
  };

  useEffect(() => {
    socket.on(`update`, (data) => {
      const newData = pollyList.map((e) => {
        if (e._id === data._id) e = data;
        return e;
      });
      setData(newData);
    });
  }, [socket, pollyList]);

  useEffect(() => {
    socket.on(`new-polly-create`, (data) => {
      pollyList.unshift(data);
      const newData = pollyList.map((e) => e);

      setData(newData);
    });
  }, [socket, pollyList]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await callHttp(
        `polly/list-polly`,
        "get",
        {},
        localStorage.getItem("token")
      );
      if (result) {
        setData(result.data);
        setLoading(false);
      }
    };

    fetchData();
  }, []);
  if (loading) {
    return loadingPage();
  }

  const handleClickItem = (e) => {
    navigate(`/polly/${e._id}`);
  };

  return (
    <>
      <div className="container" data-bs-spy="scroll">
        <div className="row">
          <button
            type="button"
            class="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            Create New Polly
          </button>
          <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">
                    New Polly
                  </h5>
                  <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div class="modal-body">
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">
                      Polly Title
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="exampleFormControlInput1"
                      placeholder="type your question here..."
                      onChange={handleTitleChange}
                    />
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">
                      Options Answers
                    </label>
                    <form>
                      {inputFields.map((inputField, index) => (
                        <div key={index} className="mb-3">
                          <input
                            type="text"
                            className="form-control"
                            placeholder={`option ${index + 1}`}
                            value={inputField}
                            onChange={(event) =>
                              handleInputChange(index, event)
                            }
                          />
                        </div>
                      ))}

                      <button
                        type="button"
                        className="btn btn-primary"
                        onClick={handleAddFields}
                      >
                        Add Option
                      </button>
                    </form>
                  </div>
                </div>
                <div class="modal-footer">
                  <button
                    type="button"
                    class="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button
                    type="button"
                    class="btn btn-primary"
                    onClick={() => handleSubmit(title, inputFields)}
                  >
                    Create
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row d-flex flex-wrap ">
          {pollyList.map((e, index) => (
            <div
              key={index}
              class="card col-md-3 col-6 shadow p-3 mb-5 bg-body rounded"
              style={{ cursor: "pointer" }}
              onClick={() => handleClickItem(e)}
            >
              <div class="card-body">
                <h5 class="card-title">Polly</h5>
                <p class="card-text">{e.title}</p>
              </div>
              <ul class="list-group list-group-flush">
                {e.options.map((e, index) => (
                  <li
                    key={index}
                    class="list-group-item d-flex justify-content-between"
                  >
                    {e.optionName}{" "}
                    <span className="border border-primary rounded-circle p-2">
                      {e.voters.length}
                    </span>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
