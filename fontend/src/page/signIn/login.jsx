import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { callHttp } from "../../API/apiCall";

export function LogIn(props) {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleNameChange = (e) => {
    setUserName(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSignIn = async (username, password) => {
    const result = await callHttp("auth/sign-in", "post", {
      email: username,
      password,
    });
    if(result){
      localStorage.setItem("token", result.token);
      navigate("/");
    }
  };

  return (
    <>
      <form class="container-lg position-absolute top-50 start-50 translate-middle">
        <div class="raw d-flex align-self-center justify-content-center align-middle">
          <div
            class="col-md-6 col-10 shadow p-3 mb-5 bg-body rounded"
            id="form"
          >
            <h1 class="text-center">Sign In</h1>
            <div class="form-floating mb-3">
              <input
                type="email"
                class="form-control email"
                id="floatingInput"
                placeholder="name@example.com"
                onChange={handleNameChange}
                required
              />
              <label for="floatingInput">Email address</label>
            </div>
            <div class="form-floating">
              <input
                type="password"
                class="form-control password"
                id="floatingPassword"
                placeholder="Password"
                onChange={handlePasswordChange}
                required
              />
              <label for="floatingPassword">Password</label>
            </div>
            <div class="button mt-3 d-flex justify-content-between">
              <button type="button" class="btn btn-success w-50" onClick={() => handleSignIn(username, password)}>
                SignIn
              </button>
              <button type="button" class="btn btn-danger w-50" onClick={() => navigate("/signUp")}>
                Create New Account
              </button>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
