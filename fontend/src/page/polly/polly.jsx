import { useState, useEffect, memo } from "react";
import { callHttp } from "../../API/apiCall";
import { useParams } from "react-router-dom";
import { loadingPage } from "../../component/loading";

export function Polly({ socket }) {
  const [polly, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [checkedVoter, setChecked] = useState(false);
  const [show, setShow] = useState(true);
  const [vote, setVote] = useState([]);
  const { pollyId } = useParams();
  const [voters, setVoters] = useState([]);
  useEffect(() => {
    socket.on(`voted_${pollyId}`, (data) => {
      setVoters(data.options);
      setChecked(true);
    });
  }, [socket, voters]);
  const handleInputChange = (event) => {
    if (event.target.checked) vote.push(event.target.value);
    else vote.splice(vote.indexOf(event.target.value), 1);
    setVote(vote);
  };

  const handleSubmitVote = async (pollyId, vote) => {
    const result = await callHttp(
      `polly/vote-polly`,
      "put",
      { pollyId, optionNames: vote },
      localStorage.getItem("token")
    );
    if (result) {
      socket.emit("new-vote", { pollyId });
      alert("You voted successfully!!");
    }
  };
  useEffect(() => {
    // Fetch data before initial render
    const fetchData = async () => {
      const result = await callHttp(
        `polly/${pollyId}`,
        "get",
        {},
        localStorage.getItem("token")
      );

      const currentUser = await callHttp(
        `user/my-profile`,
        "get",
        {},
        localStorage.getItem("token")
      );
      if (result && currentUser) {
        if (
          (currentUser && currentUser.votedPolly.includes(pollyId)) ||
          currentUser._id === result.owner._id
        )
          setChecked(true);
        if (currentUser._id === result.owner._id) setShow(false);
        setData(result);
        setVoters(result.options);
        setLoading(false);
      }
    };

    fetchData(); // Call the fetchData function
  }, []); // Empty dependency array makes this run only on mount

  if (loading) {
    return loadingPage();
  }
  return (
    <>
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-12 justify-content-center">
            <div class="card">
              <div class="card-body m-1">
                <h5 class="card-title">Polly</h5>
                <p class="card-text">{polly.title}</p>
              </div>
              {show && (
                <div className="border-top">
                  <ul class="list-group list-group-flush">
                    {polly.options.map((e, index) => (
                      <li key={index} class="list-group-item">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          id="flexCheckDefault"
                          value={e.optionName}
                          onChange={(event) => handleInputChange(event)}
                        />
                        <label
                          class="form-check-label"
                          for="flexCheckDefault"
                          style={{ marginLeft: 5 }}
                        >
                          {e.optionName}
                        </label>
                      </li>
                    ))}
                  </ul>
                  <button
                    type="button"
                    class="btn btn-success w-100"
                    onClick={() => handleSubmitVote(pollyId, vote)}
                  >
                    Vote
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
        {checkedVoter && (
          <div className="row d-flex justify-content-center mt-3">
            <div className="col-12 d-flex flex-column">
              {voters.map((e, index) => (
                <div
                  key={index}
                  className="mb-2 shadow-sm p-3 bg-body rounded border border-primary border-2 "
                >
                  <h5>{e.optionName}</h5>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-striped text-white"
                      role="progressbar"
                      style={{
                        width: `${
                          (e.voters.length /
                            voters.reduce((i, e) => i + e.voters.length, 0)) *
                          100
                        }%`,
                      }}
                      aria-valuenow="10"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      {e.voters.length}
                    </div>
                  </div>
                  <div>
                    {e.voters.map((v, index) => (
                      <span key={index} className="m-1">
                        <b>{v.fullName}</b>
                      </span>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    </>
  );
}
