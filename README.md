## Back-end

```bash
# run app
$ cd backend/
$ run command docker-compose up --build
```

## Font-end
```bash
# run app
$ cd fontend/
$ yarn install
$ yarn start